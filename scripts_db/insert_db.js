const knex = require('knex')
const config = require('../db/knexfile.js')

main()

async function main() {
   try {
      let db = knex(config.development)

      await db.into('spots').insert({nom: "Pech David", longitude: "1.44642", latitude: "43.5586", description:"Situé à 1h50 environ de Toulouse en Ariège, ce site t'offre la possibilité de contempler un ciel magnifique ! La nuit, aucune lumière ne parasite ce site grâce aux sommets environnants protégeant bien des lueurs gênantes à l'observation. Attention, si tu t'éloignes de la route pour atteindre le col, quelques troupeaux se promènent de part et d'autre dans les hauteurs.", photo: "https://picsum.photos/200/300"})
      await db.into('spots').insert({nom: "Pic du Midi", longitude: "0.141092", latitude: "42.936872", description:"C’est l’un des sites les plus réputés pour pouvoir observer les étoiles ! Ta soirée commence par une montée en téléphérique pour enfin pouvoir observer le coucher du soleil et la naissance des étoiles depuis la terrasse panoramique. Réserve vite ta place pour 47€ la soirée ! Décolage assuré…", photo: "https://picsum.photos/200/300"})
      await db.into('spots').insert({nom: "Cité de l'espace", longitude: "1.493357", latitude: "43.586627", description:"Tous les vendredis soirs de 21h à 21h30 - sauf dernier vendredi du mois, conférence.", photo: "https://picsum.photos/200/300"})
      await db.into('spots').insert({nom: "Observatoire de Jolimont", longitude: "1.278", latitude: "43.367", description:"Découvre de vrais engins spatiaux, les secrets de l’espace, entraine toi comme un astronaute, et admire les étoiles autant de fois que tu le souhaites ! Tu peux profiter de la cité à partir de 21€ pour une journée.", photo: "https://picsum.photos/200/300"})

      await db.into('sky_events').insert({
         nom: "Une éclipse totale de Lune.",
         date: "26/05/2021",
         description:"Si 2020 a été avare en éclipses de Lune spectaculaires, l'année nous en a offert quatre, mais toutes étaient « par la pénombre », 2021 s'annonce plus généreuse en la matière. Une éclipse totale est en effet attendue le 26 mai prochain. La Lune passera alors entièrement dans l'ombre de la Terre.", 
      })
      
      await db.into('sky_events').insert({
         nom: "Une super Lune au mois de mai.",
         date: "27/04/2021",
         description:"Chaque année, les amateurs d'astronomie guettent la super Lune. Elle se produit lorsque la Lune est pleine et qu'elle se trouve, en même temps, au plus proche de la Terre. En 2021, cela se produira le 26 mai avec une Lune à quelque 357.309 kilomètres seulement de la Terre. Fortes marées en perspective. Le 27 avril 2021, la Lune se trouvera à peine plus loin de notre Planète, à 357.378 kilomètres. De quoi nous offrir également une magnifique pleine Lune.", 
      })
      
      await db.into('sky_events').insert({
         nom: "Une éclipse annulaire du Soleil en juin.",
         date: "10/06/2021",
         description:"Le 10 juin 2021, c'est le nord de l'hémisphère nord de notre Planète qui se prépare à assister à une éclipse de Soleil annulaire. Un genre d'éclipse qui se produit lorsque le diamètre apparent de la Lune n'est pas aussi grand que celui du Soleil. Les plus chanceux observent alors une sorte d'anneau de feu autour de notre satellite naturel. Cette fois, la bande de centralité commencera du côté du nord-est du Canada pour traverser le Groenland par le nord-ouest, passer par le pôle Nord et finir sur l'est de la Russie.", 
      })
      
      await db.into('sky_events').insert({
         nom: "Une conjonction Mars-Venus au cœur de l’été.",
         date: "12/07/2021",
         description:"Le 21 décembre 2020, Jupiter et Saturne se sont approchées l'une de l'autre dans le ciel terrestre. En apparence du moins. Dans la nuit du 12 juillet 2021, ce sera au tour de Vénus et de Mars de se rencontrer et de nous offrir un spectacle semblable. Les deux planètes apparaîtront en même temps dans les télescopes. À l'œil nu, la conjonction sera facile à observer avec une Vénus qui brillera fortement dans le ciel comme à son habitude. Pour les amateurs de photographie astronomique, sachez que la Lune se joindra au spectacle, offrant un joli croissant juste à côté de la conjonction. D'autres conjonctions, un peu moins rapprochées auront lieu le 5 mars 2021, entre Mercure et Jupiter, le 10 mars entre Mercure, Jupiter et Saturne, le 25 avril entre Mercure et Vénus, le 12 mai entre Vénus et la Lune et le 18 août entre Mercure et Mars.", 
      })
      
      process.exit(0)
   } catch(err) {
      console.log(err.toString())
   }
}