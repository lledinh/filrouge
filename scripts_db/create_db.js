const knex = require('knex')
const config = require('../db/knexfile.js')

main()

async function main() {
   try {
      let db = knex(config.development)

      let usersTableExists = await db.schema.hasTable('users')
      if (usersTableExists) {
         console.log('table "users" already exists')
      } else {
         await db.schema.createTable('users', table => {
            table.increments('id')
            table.text('fullname').notNull()
            table.string('email').unique().notNull()
            table.string('password').notNull()
            table.timestamp('created_at').defaultTo(db.fn.now())
         })
         console.log('table "users" created')

         await db.schema.createTable('spots', table => {
            table.increments('id')
            table.string('id_user')
            table.string('nom').notNull()
            table.string('longitude').notNull()
            table.string('latitude').notNull()
            table.text('description')
            table.text('photo')
            table.timestamp('created_at').defaultTo(db.fn.now())
         })
         console.log('table "spots" created')

         await db.schema.createTable('pictures', table => {
            table.increments('id')
            table.string('id_spot')
            table.string('url')
            table.timestamp('created_at').defaultTo(db.fn.now())
         })
         console.log('table "pictures" created')


         await db.schema.createTable('comments', table => {
            table.increments('id')
            table.string('id_spot')
            table.string('id_user')
            table.text('description')
            table.timestamp('created_at').defaultTo(db.fn.now())
         })
         console.log('table "comments" created')


         await db.schema.createTable('evaluation', table => {
            table.increments('id')
            table.string('id_spot')
            table.string('id_user')
            table.string('value')
            table.timestamp('evaluation').defaultTo(db.fn.now())
         })
         console.log('table "comments" created')

         await db.schema.createTable('frequentation', table => {
            table.increments('id')
            table.string('id_spot')
            table.string('id_user')
            table.string('value')
            table.timestamp('created_at').defaultTo(db.fn.now())
         })
         console.log('table "frequentation" created')

         await db.schema.createTable('tools', table => {
            table.increments('id')
            table.string('id_spot')
            table.text('description')
            table.string('quantity')
            table.timestamp('created_at').defaultTo(db.fn.now())
         })
         console.log('table "tools" created')


         await db.schema.createTable('sky_events', table => {
            table.increments('id')
            table.string('nom')
            table.text('description')
            table.string('date')
            table.timestamp('created_at').defaultTo(db.fn.now())
         })
         console.log('table "sky_events" created')
      }
      process.exit(0)
   } catch(err) {
      console.log(err.toString())
   }
}