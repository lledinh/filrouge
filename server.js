const knex = require('knex');
const path = require('path')
const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const service = require('feathers-knex')
const knexFeathersServices = require('./db/knex_feathers_services')
const cors = require('cors')
const app = express(feathers())
const userService = require ('./userService')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const cookieParser = require("cookie-parser")


// const createAccount = require('./middlewares/create-account')

app.engine('pug', require('pug').__express)

app.set("views", path.join(__dirname, "views"))
app.set("view engine", "pug")

app.use(express.static(path.join(__dirname, "public")))

app.use(cors())

app.use(cookieParser())

app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.get("/api/spots", async (req,res) => {
    const spots = await knexFeathersServices.spotService.find({});
    res.send(spots);
});

app.get("/api/sky_events", async (req,res) => {
    const skyEvents = await knexFeathersServices.skyEventsService.find({});
    res.send(skyEvents);
});

app.post('/signin', async (data, res) => {
    let email = data.body.email
    let password = data.body.password
    const user = await knexFeathersServices.userService.find({ 
        query: {email: email}
    })
     try {        
        if (user[0].password && bcrypt.compareSync(password, user[0].password)) {
            const token = jwt.sign({ userId: user[0].id }, "vjfhwAErorwBP8cFiCvb", {
                algorithm: "HS256",
                expiresIn: 3600,
            })
            res.cookie("token", token, { maxAge: 3600 * 1000 })
            res.send(200)
        }
        else res.send(403, "Mot de Passe Invalide")
    }
    catch {
        res.send(404, "Email Invalide")
    }
 });

 app.post('/create-account', async (data, res) => {
        let email = data.body.email
        let name = data.body.name
        let password = data.body.password

        console.log(email,name,password, data.body)

        const saltRounds = 10
        const hash = bcrypt.hashSync(password, saltRounds)
        // Création du compte
        try {
            await knexFeathersServices.userService.create({fullname: name, email: email, password: hash})
            console.log("Compte créé")
            res.send(200)
        }
        catch(error) {
            console.log(error)
            console.log("Email déjà utilisé")
            res.send(403, "Email déjà utilisé")
        }
 });

 app.post('/add-spot', async data => {
     let spot = data.body
     let token = jwt.verify(data.cookies.token, "vjfhwAErorwBP8cFiCvb")
     let id_user = token.userId

     console.log(spot,id_user)

     try {
        await knexFeathersServices.spotService.create({
            id_user: id_user, nom: spot.nom, longitude: spot.longitude, latitude: spot.latitude, 
            description: spot.description, photo: spot.photo})
            console.log('ça marche')
     }
     catch (e) {
        console.log('ça marche pas')
     }
     })

app.listen(3030, function () {
    console.log("Server listening on port 3030")
})
  
  
