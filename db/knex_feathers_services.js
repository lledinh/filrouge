const service = require('feathers-knex')
const knex = require('knex')

let config = require('./knexfile.js')
let database = knex(config.development)
let spotService = service({Model: database, name: 'spots'})
let skyEventsService = service({Model: database, name: 'sky_events'})
let userService = service({Model: database, name: 'users'})


module.exports = {
    spotService,
    skyEventsService,
    userService
}
