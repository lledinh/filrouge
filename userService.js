const service = require('feathers-knex')
const knex = require('knex')

let config = require('./knexfile.js')
let database = knex(config.development)
let userService = service({Model: database, name: 'users'})

module.exports = {
    userService
}